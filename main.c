#include <util/delay.h>
#include "ili9163c.h"
#include "gameplay.h"
#include "graphic.h"
#include "input.h"

#define GAME_LOOP_TIME 100

int main(void)
{
	player player1;
	player player2;
	player1.color = rgb(31,0,0);
	player2.color = rgb(0,0,31);

    lcdInitialise(LCD_ORIENTATION2);
	buttonsInitialize();
	printArena();
	makePlayers(&player1, &player2);
		
	while(1)
	{
		if (checkHit(&player2) || !(checkBorders(&player2))) {
			player1.isWinner = TRUE;
		}
		
		if (checkHit(&player1) || !(checkBorders(&player1))) {
			player2.isWinner = TRUE;
		}
		
		if (player1.isWinner && player2.isWinner) {
			printDraw();
			break;
		} else if (player1.isWinner) {
			printWinner(&player1);
			break;
		} else if (player2.isWinner) {
			printWinner(&player2);
			break;
		}
		
		printPlayer(&player1);
		printPlayer(&player2);
		
		// Player one turn left button pin check
		if (checkLeft(0x40, &player1)) 
			turnLeft(&player1);
		
		// Player one turn right button pin check
		if (checkRight(0x10, &player1)) 
			turnRight(&player1);
		
		// Player one turn left button pin check
		if (checkLeft(0x20, &player2)) 
			turnLeft(&player2);
		
		// Player two turn right button pin check
		if (checkRight(0x01, &player2)) 
			turnRight(&player2);
		
		movePlayer(&player1);
		movePlayer(&player2);
		
		_delay_ms(GAME_LOOP_TIME);
		
	}
}