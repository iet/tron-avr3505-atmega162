#include "input.h"

/**
Initilize buttons port to be input with power from pin.
*/
void buttonsInitialize() {
    DDRC  = 0x00;
    PORTC = 0xFF;
}

uint8_t checkLeft(uint8_t pin, player *ply) {

	if(!(PINC & pin) && (ply->isLeftClicked==FALSE))
	{
		ply->isLeftClicked=TRUE;
		return TRUE;
	}
	
	if((PINC & pin) && (ply->isLeftClicked==TRUE)) {
		ply->isLeftClicked=FALSE;
		return FALSE;
	}
	
	return FALSE;
}

uint8_t checkRight(uint8_t pin, player *ply) {

	if(!(PINC & pin) && (ply->isRightClicked==FALSE))
	{
		ply->isRightClicked=TRUE;
		return TRUE;
	}
	
	if((PINC & pin) && (ply->isRightClicked==TRUE)) {
		ply->isRightClicked=FALSE;
		return FALSE;
	}
	
	return FALSE;
}
