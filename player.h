#include <stdint.h>

#ifndef PLAYER_H
#define PLAYER_H

#define TRUE 1
#define FALSE 0

/**
Enumeration with possible directions
*/
enum direction
{
	LEFT, UP, DOWN, RIGHT
};

struct player
{
	uint8_t x, y; // Player x,y position
	enum direction drtn; // Player direction
	uint8_t isRightClicked; // TRUE if user hold right button
	uint8_t isLeftClicked; // TRUE if user hold left button
	uint16_t color; // Player color
	uint8_t isWinner; // TRUE if user is winner
};

typedef struct player player;

#endif