#include "gameplay.h"

void movePlayer(player* ply) {
	arena[ply->x/16][ply->y/2] |= 1 << (ply->x/2)%8;
	switch (ply->drtn) {
   		case LEFT: 
   			ply->x-=2;
     		break;
   		case DOWN: 
   			ply->y-=2;
     		break;
   		case RIGHT: 
   			ply->x+=2;
     		break;
   		case UP: 
   			ply->y+=2;
     		break;
	   	default: 
	    	break;
 	}
	
}

void turnLeft(player *ply) {
	switch (ply->drtn) {
   		case LEFT: 
   			ply->drtn = DOWN;
     		break;
   		case DOWN: 
   			ply->drtn = RIGHT;
     		break;
   		case RIGHT: 
   			ply->drtn = UP;
     		break;
   		case UP: 
   			ply->drtn = LEFT;
     		break;
	   	default: 
	    	break;
 	}
}

void turnRight(player *ply) {
	switch (ply->drtn) {
   		case LEFT: 
   			ply->drtn = UP;
     		break;
   		case DOWN: 
   			ply->drtn = LEFT;
     		break;
   		case RIGHT: 
   			ply->drtn = DOWN;
     		break;
   		case UP: 
   			ply->drtn = RIGHT;
     		break;
	   	default: 
	    	break;
 	}
}


uint8_t checkBorders(player* ply) {
		if (ply->x>=127 || ply->y>=127
			|| ply->x<=0 || ply->y<=0) 
			return FALSE;
		else
			return TRUE;
}

uint8_t checkHit(player* ply) {
	uint8_t row = arena[ply->x/16][ply->y/2];
	if (row &= 1 << ((ply->x/2)%8)) {
		return TRUE;
	} else {
		return FALSE;
	}
}


void makePlayers(player* ply1, player* ply2) {
	ply1->x = 10;
	ply1->y = 64;
	ply1->drtn = RIGHT;
	ply1->isLeftClicked = FALSE;
	ply1->isRightClicked = FALSE;
	ply1->isWinner = FALSE;
	
	ply2->x = 120;
	ply2->y = 64;
	ply2->drtn = LEFT;
	ply2->isLeftClicked=FALSE;
	ply2->isRightClicked = FALSE;
	ply2->isWinner = FALSE;
}

