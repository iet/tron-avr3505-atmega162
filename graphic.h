#include <stdint.h>
#include "ili9163c.h"
#include "player.h"

/**
Change r,g and b values to single value
*/
inline uint16_t rgb(uint8_t r, uint8_t g, uint8_t b)
{
    return (b << 11) | (g << 6) | (r);
}   

/**
Prints medal for a player.
It uses player color in medal.
*/
void printWinner(player* player);

/**
Prinst player on his position.
*/
void printPlayer(player* player);

/**
Use it for debug only. 
It prints player as single dot.
Usefull with collision detect.
*/
void printPlayerDebug(player* player);

/**
Prints games arena
*/
void printArena(void);

/**
Prints screen representing draw.
*/
void printDraw(void);