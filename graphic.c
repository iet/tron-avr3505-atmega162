#include "graphic.h"

void printWinner(player* ply) {
	lcdClearDisplay(rgb(28, 28, 28));
	
	for(int i = 1; i<=27; i++) {
		lcdCircle(64,80,i,ply->color);
	}
	
	lcdLine(20,5,60,45,rgb(0,31,0));
	lcdLine(19,5,59,45,rgb(0,31,0));
	lcdLine(18,5,58,45,rgb(0,31,0));
	
	lcdLine(67,45,107,5,rgb(0,31,0));
	lcdLine(68,45,108,5,rgb(0,31,0));
	lcdLine(69,45,109,5,rgb(0,31,0));
	
	lcdCircle(64,80,30,rgb(31,31,00));
	lcdCircle(64,80,29,rgb(31,31,00));
	lcdCircle(64,80,28,rgb(31,31,00));
}

void printDraw() {
	lcdClearDisplay(rgb(31, 0, 31));
}

void printPlayer(player *ply) {
		lcdRectangle(ply->x, ply->y, ply->x+1, ply->y+1, ply->color);
}

void printPlayerDebug(player *ply) {
		lcdPlot(ply->x, ply->y, ply->color);
}

void printArena() {
    lcdClearDisplay(rgb(0, 0, 0));
	lcdRectangle(0, 0, 127, 127, rgb(31,31,31));
	lcdRectangle(1, 1, 126, 126, rgb(31,31,31));
}