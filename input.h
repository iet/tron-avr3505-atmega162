#include <avr/io.h>
#include "player.h"

/**
Initilize buttons port C to be input with power from pin.
*/
void buttonsInitialize(void);

/**
Returns TRUE if user clicked left button. 
It checks if button wasn't pressed and not released, 
becouse we don't want to turn left in the loop.
*/
uint8_t checkLeft(uint8_t pin, player *ply);

/**
Returns TRUE if user clicked right button. 
It checks if button wasn't pressed and not released, 
becouse we don't want to turn right in the loop.
*/
uint8_t checkRight(uint8_t pin, player *ply);
