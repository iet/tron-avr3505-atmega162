#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <string.h>

#include "ili9153.h"

// Pin configurations on port B

#define RST 3
#define CS 4
#define SIO 5
#define SCK 7
#define LED 0
#define DC 2

#define RST_1 	PORTB |= (1<<PB3)
#define RST_0 	PORTB &= ~(1<<PB3)

#define LED_1 	PORTB |= (1<<PB0)
#define LED_0 	PORTB &= ~(1<<PB0)

#define DC_1 	PORTB |= (1<<PB2)
#define DC_0  	PORTB &= ~(1<<PB2)

#define CS_1 	PORTB |= (1<<PB4)
#define CS_0   	PORTB &= ~(1<<PB4)


// Reset the LCD, reset pin is active low
void lcdReset(void)
{
    RST_0;
    _delay_ms(50);

    RST_1;
    _delay_ms(120);
}

// Write command to LCD via 4 wire SPI
void lcdWriteCommand(uint8_t address)
{
	// Command
	DC_0; 
	CS_0;
	
	// Start transmission and wait for it's end
	SPDR = address;
	while(!(SPSR & (1<<SPIF)));
		
	CS_1;
}

// Write 8 bit data to LCD via 4 wire SPI
void lcdWriteData(uint8_t data)
{  
	// Data
	DC_1;
	CS_0;
	
	// Start transmission and wait for it's end
	SPDR = data;
	while(!(SPSR & (1<<SPIF)));
		
	CS_1;
}
 
// Write 16 bit data to LCD via 4 wire SPI
void lcdWriteData16(uint8_t data1, uint8_t data2)
{  
	// Data
	DC_1;
	CS_0;
	
	// Start transmission and wait for it's end
	SPDR = data1;
	while(!(SPSR & (1<<SPIF)));
	
	// Start transmission and wait for it's end
	SPDR = data2;
	while(!(SPSR & (1<<SPIF)));
	
	CS_1;
}

// Init screen with given orientation
void lcdInitialise(uint8_t orientation)
{   
    // Clear the control pins
	DDRB = (1<<RST | 1<<SCK | 1<<SIO | 1<<CS | 1<<LED | 1<<DC);
	PORTB = 1<<SCK | 1<<SIO | 1<<CS | 1<<LED;
	
	// Init SPI
	SPCR = 1<<SPE | 1<<MSTR | 1<<CPOL | 1<<CPHA;;
	SPSR = 1<<SPI2X;
    
    // Reset the LCD
    lcdReset();
	
    // Wait for the screen to wake up
    lcdWriteCommand(EXIT_SLEEP_MODE);
    _delay_ms(5); 
    
	// Set 16 bits mode per pixel
    lcdWriteCommand(SET_PIXEL_FORMAT);
    lcdWriteData(0x05);  
	
    // Set gamma curve 3
    lcdWriteCommand(SET_GAMMA_CURVE);
    lcdWriteData(0x04); 
	
    // Set gamma adjustment enabled
    lcdWriteCommand(GAM_R_SEL);
    lcdWriteData(0x01);
	
	// Set positive gamma correction and it's 15 parameters
    lcdWriteCommand(POSITIVE_GAMMA_CORRECT);
    lcdWriteData(0x3f); 
    lcdWriteData(0x25); 
    lcdWriteData(0x1c); 
    lcdWriteData(0x1e); 
    lcdWriteData(0x20); 
    lcdWriteData(0x12); 
    lcdWriteData(0x2a); 
    lcdWriteData(0x90); 
    lcdWriteData(0x24); 
    lcdWriteData(0x11); 
    lcdWriteData(0x00); 
    lcdWriteData(0x00); 
    lcdWriteData(0x00); 
    lcdWriteData(0x00); 
    lcdWriteData(0x00); 
     
	// Set negative gamma correction and it's 15 parameters
    lcdWriteCommand(NEGATIVE_GAMMA_CORRECT);
    lcdWriteData(0x20); 
    lcdWriteData(0x20); 
    lcdWriteData(0x20); 
    lcdWriteData(0x20); 
    lcdWriteData(0x05); 
    lcdWriteData(0x00); 
    lcdWriteData(0x15); 
    lcdWriteData(0xa7); 
    lcdWriteData(0x3d); 
    lcdWriteData(0x18); 
    lcdWriteData(0x25); 
    lcdWriteData(0x2a); 
    lcdWriteData(0x2b);
    lcdWriteData(0x2b); 
    lcdWriteData(0x3a);
    
	// Set frame, power and communication options
    lcdWriteCommand(FRAME_RATE_CONTROL1);
    lcdWriteData(0x08); 
    lcdWriteData(0x08); 
    
    lcdWriteCommand(DISPLAY_INVERSION);
    lcdWriteData(0x07); 
   
    lcdWriteCommand(POWER_CONTROL1);
    lcdWriteData(0x0a); 
    lcdWriteData(0x02); 
      
    lcdWriteCommand(POWER_CONTROL2);
    lcdWriteData(0x02); 

    lcdWriteCommand(VCOM_CONTROL1);
    lcdWriteData(0x50); 
    lcdWriteData(0x5b); 
    
    lcdWriteCommand(VCOM_OFFSET_CONTROL);
    lcdWriteData(0x40);  
    
	// Set screen sizes
    lcdWriteCommand(SET_COLUMN_ADDRESS);
    lcdWriteData(0x00);
    lcdWriteData(0x00); 
    lcdWriteData(0x00); 
    lcdWriteData(0x7f); 
   
    lcdWriteCommand(SET_PAGE_ADDRESS);
    lcdWriteData(0x00);
    lcdWriteData(0x00);
    lcdWriteData(0x00);
    lcdWriteData(0x7f); 
    
    // Set display orientation
    lcdWriteCommand(SET_ADDRESS_MODE);
    lcdWriteData(orientation);

    // Display to on and wait for commands
    lcdWriteCommand(SET_DISPLAY_ON);
    lcdWriteCommand(WRITE_MEMORY_START);
}


void lcdClearDisplay(uint16_t colour)
{
    uint16_t pixel;
  
    // Select all x screen (0-127)
    lcdWriteCommand(SET_COLUMN_ADDRESS);
    lcdWriteData(0x00);
    lcdWriteData(0x00);
    lcdWriteData(0x00);
    lcdWriteData(0x7f);

    // Select all y screen (0-127)
    lcdWriteCommand(SET_PAGE_ADDRESS);
    lcdWriteData(0x00);
    lcdWriteData(0x00);
    lcdWriteData(0x00);
    lcdWriteData(0x7f);
  
    // Draw all pixels
    lcdWriteCommand(WRITE_MEMORY_START);
    for(pixel = 0; pixel < 16385; pixel++) 
		lcdWriteData16(colour >> 8, colour);
}

// Draw single pixel
void lcdPlot(uint8_t x, uint8_t y, uint16_t colour)
{
    // Horizontal Address Start Position
    lcdWriteCommand(SET_COLUMN_ADDRESS);
    lcdWriteData(0x00);
    lcdWriteData(x);
    lcdWriteData(0x00);
    lcdWriteData(0x7f);
  
    // Vertical Address end Position
    lcdWriteCommand(SET_PAGE_ADDRESS);
    lcdWriteData(0x00);
    lcdWriteData(y);
    lcdWriteData(0x00);
    lcdWriteData(0x7f);

    // Plot the point
    lcdWriteCommand(WRITE_MEMORY_START);
    lcdWriteData16(colour >> 8, colour);
}

// Draw a line from x0, y0 to x1, y1 using Bresenham's line drawing algorithm
void lcdLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t colour)
{
    int16_t dy = y1 - y0;
    int16_t dx = x1 - x0;
    int16_t stepx, stepy;

    if (dy < 0)
    {
        dy = -dy; stepy = -1; 
    }
    else stepy = 1; 

    if (dx < 0)
    {
        dx = -dx; stepx = -1; 
    }
    else stepx = 1; 

    dy <<= 1;                           // dy is now 2*dy
    dx <<= 1;                           // dx is now 2*dx
 
    lcdPlot(x0, y0, colour);

    if (dx > dy) {
        int fraction = dy - (dx >> 1);  // same as 2*dy - dx
        while (x0 != x1)
        {
            if (fraction >= 0)
            {
                y0 += stepy;
                fraction -= dx;         // same as fraction -= 2*dx
            }

            x0 += stepx;
            fraction += dy;                 // same as fraction -= 2*dy
            lcdPlot(x0, y0, colour);
        }
    }
    else
    {
        int fraction = dx - (dy >> 1);
        while (y0 != y1)
        {
            if (fraction >= 0)
            {
                x0 += stepx;
                fraction -= dy;
            }

            y0 += stepy;
            fraction += dx;
            lcdPlot(x0, y0, colour);
        }
    }
}

// Draw a rectangle between x0, y0 and x1, y1
void lcdRectangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t colour)
{
    lcdLine(x0, y0, x0, y1, colour);
    lcdLine(x0, y1, x1, y1, colour);
    lcdLine(x1, y0, x1, y1, colour);
    lcdLine(x0, y0, x1, y0, colour);
}

// Draw a filled rectangle
void lcdFilledRectangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t colour)
{
    uint16_t pixels;
	
    // X address start position
    lcdWriteCommand(SET_COLUMN_ADDRESS); 
    lcdWriteData(0x00);
    lcdWriteData(x0);
    lcdWriteData(0x00);
    lcdWriteData(x1);
	
	// Y address end position
    lcdWriteCommand(SET_PAGE_ADDRESS);
    lcdWriteData(0x00);
    lcdWriteData(y0);
    lcdWriteData(0x00);
    lcdWriteData(0x7f);
        
    lcdWriteCommand(WRITE_MEMORY_START);
    
    for (pixels = 0; pixels < ((x1 - x0) * (y1 - y0)); pixels++)
        lcdWriteData16(colour >> 8, colour);;
}


// Draw a circle using Bresenham's line drawing algorithm. 
void lcdCircle(int16_t xCentre, int16_t yCentre, int16_t radius, uint16_t colour)
{
    int16_t x = 0, y = radius;
    int16_t d = 3 - (2 * radius);
 
    while(x <= y)
    {
        lcdPlot(xCentre + x, yCentre + y, colour);
        lcdPlot(xCentre + y, yCentre + x, colour);
        lcdPlot(xCentre - x, yCentre + y, colour);
        lcdPlot(xCentre + y, yCentre - x, colour);
        lcdPlot(xCentre - x, yCentre - y, colour);
        lcdPlot(xCentre - y, yCentre - x, colour);
        lcdPlot(xCentre + x, yCentre - y, colour);
        lcdPlot(xCentre - y, yCentre + x, colour);

        if (d < 0) d += (4 * x) + 6;
        else
        {
            d += (4 * (x - y)) + 10;
            y -= 1;
        }

        x++;
    }
}