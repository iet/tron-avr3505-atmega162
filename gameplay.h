#include <stdint.h>
#include "player.h"

uint8_t arena[8][64];

/**
Moves player forward to it's direction
*/
void movePlayer(player* ply);

/**
Turns player left.
*/
void turnLeft(player* ply);

/**
Turns player right
*/
void turnRight(player* ply);

/**
Prepare two players.
Resets players fields and status.
*/
void makePlayers(player* ply1, player* ply2);

/**
Checks if player hits a border.
Returns TRUE if user is outside arena.
*/
uint8_t checkBorders(player* ply);

/**
Checks if user hits a wall.
Returns TRUE if user is loser.
*/
uint8_t checkHit(player* ply);